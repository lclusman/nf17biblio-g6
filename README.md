# Projet Biblio NF17 - Groupe 6

Projet de NF17 Biblio du groupe D5-G6

## Membres du groupe

- Bastian COSSON
- Lucas CLUSMAN
- Wenyi JIANG
- Sosthène LEROY
- Thomas NEVEUX

## Informations

Pour créer et remplir la base de données correctement, il faut exécuter les 
fichiers SQL dans l'ordre suivant:

- create.sql
- insertions.sql
- vues.sql
 

Le fichier create.sql contient le code créant les tables de la base de données.
Le fichier insertions.sql insère les données dans les tables.
Le fichier vues.sql crée des vues qui pourront être utilisées du côté applicatif
à la suite du projet.
