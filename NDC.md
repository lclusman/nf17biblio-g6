# Note de Clarification

Version 1

Le 28/03/2020

## Contexte

Le projet porte la conception d'un système de gestion pour une bibliothèque municipale, souhaitant informatiser ses activités.

## Acteurs

Maîtrise d’ouvrage: la bibliothèque municipale

Maîtrise d’oeuvre:

- Bastian Cosson
- Lucas Clusman
- Thomas Neveux
- Wenyi Jiang
- Sosthène Leroy

## Objet du projet

Le projet consiste à faciliter faciliter aux adhérents la recherche de documents et la gestion de leurs emprunts, ainsi que la facilitation de la gestion des ressources documentaires. La gestion des utilisateurs et de leurs données est également un des objectifs pour aider le personnel, et également la gestion des prêts, des retards et des réservations.
Il faudra également établir des statistiques en vue de déterminer les documents populaires et les profils des adhérents.

## Livrables

- Note de clarification
- Modèle UML du projet
- Le MCD
- Le MLD
- Les fichiers de code du projet

## Objets

- Ressource
- Contributeur
- Contribution
- Membre
- Adhérent
- Prêt
- Sanction

## Propriétés

- Une ressource possède un type, un code unique l’identifiant, un titre, une date d’apparition, un éditeur, un genre, un code de classification pour le localiser, la liste des contributeurs, l’état.
- Un contributeur a un nom, un prénom, une date de naissance et la nationalité.
- Suivant le type de ressource, le contributeur sera d’un type différent.
- Il peut y avoir plusieurs contributeurs pour une même ressource.
- Un membre du personnel a un login, un mot de passe, un nom, prénom, une adresse, et un mail.
- Un adhérent à la bibliothèque un les mêmes attributs qu’un membre du personnel, avec en plus son numéro de téléphone et un attribut blacklist, qui est utile puisque l’on veut savoir si l’adhérent a le droit d’emprunter un livre.
- Suivant le type de document, des spécifications doivent être ajoutés : L’ISBN pour un livre et son résumé, la langue des documents écrits et des films, etc.
- Un prêt est caractérisé par sa date de prêt et sa durée.

## Contraintes

- Un adhérent doit s’authentifier pour emprunter un document.
- Chaque prêt doit être rendu en bon état, dans les temps, sans quoi l’adhérent aura une sanction.
- Un membre du personnel peut décider blacklister un adhérent qui aurait eu trop de sanctions.
- Nous devons garder la trace de toutes les adhésions, actuelles et passées.
- Un document doit être disponible et en bon état pour être prêté.
- Si le document est gravement détérioré, l’adhérent doit le rembourser sinon il ne pourra plus rien emprunter.
- Il faut mettre en place un système permettant de déterminer les documents populaires, de manière générale et par adhérent.

## Utilisateurs

- Un adhérent a le droit d’emprunter, et de parcourir les documents.
- Un membre du personnel a des fonctionnalités d’administration, soit lire et modifier la base de données de documents.
- Un membre peut également blacklister ou sanctionner un adhérent.
