-- INSERTIONS

INSERT INTO compte VALUES ('lclusman','1234');
INSERT INTO compte VALUES ('scrozat','2233');
INSERT INTO compte VALUES ('wjiang','1100');
INSERT INTO compte VALUES ('sleroy','1100');
INSERT INTO compte VALUES ('bcosson','1100');
INSERT INTO compte VALUES ('tneveux','1100');

INSERT INTO membre VALUES ('clusman','lucas','lclusman','4 boulevard Victor Hugo','lclusman@utc.fr');
INSERT INTO membre VALUES ('crozat','stephane','scrozat','10 ceci cela','scrozat@utc.fr');
INSERT INTO membre VALUES ('leroy','sosthene','sleroy','10 cecaaaai cela','sleroy@utc.fr');

-- Je mets des dates de naissance aléatoires

INSERT INTO adherent VALUES ('jiang','wenyi','wjiang','13-04-99','tes test','wjiang@utc.fr','0610498721',FALSE,'20-03-2020',30);
INSERT INTO adherent VALUES ('cosson','bastian','bcosson','14-05-99','tes test','bcosson@utc.fr','0616478721',FALSE,'15-03-2020',30);
INSERT INTO adherent VALUES ('neveux','thomas','tneveux','15-06-99','tes test','tneveux@utc.fr', '0617722821',FALSE,'10-03-2020',30);

INSERT INTO contributeur VALUES ('hugo','victor','26-02-1802','francais');
INSERT INTO contributeur VALUES ('baudelaire','charles','06-04-1821','francais');
INSERT INTO contributeur VALUES ('shakespeare','william','26-04-1564','britannique');
INSERT INTO contributeur VALUES ('spielberg','steven','18-12-1946','americaine');
INSERT INTO contributeur VALUES ('besson','luc','18-03-1959','francais');
INSERT INTO contributeur VALUES ('kubrick','stanley','26-07-1928','americaine');
INSERT INTO contributeur VALUES ('nicholson','jack','22-04-1937','americaine');
INSERT INTO contributeur VALUES ('ford','harrisson','13-07-1942','americaine');
INSERT INTO contributeur VALUES ('dassin','joe','20-08-1980','francais');
INSERT INTO contributeur VALUES ('gainsbourg','serge','02-04-1928','francais');

-- LIVRES
INSERT INTO ressource VALUES (1,'les miserables','1862','gallimard','realisme',1354);
INSERT INTO ressource VALUES (2,'notre dame de paris','1831','gallimard','historique',1254);
INSERT INTO ressource VALUES (3,'les travailleurs de la mer','1866','gallimard','roman',1666);
INSERT INTO ressource VALUES (4,'les paradis artificiels','1860','editeur1','essai',1154);
INSERT INTO ressource VALUES (5,'les fleurs du mal','1857','gallimard','posesie',1284);
INSERT INTO ressource VALUES (6,'romeo et juliette','1597','gallimard','theatre',1954);
INSERT INTO ressource VALUES (7,'hamlet','1603','editions','theatre',1854);

-- FILMS
INSERT INTO ressource VALUES (8,'indiana jones','1981','lucasfilm','aventure',6699);
INSERT INTO ressource VALUES (9,'jurassic park','1990','universal','science-fiction',8877);
INSERT INTO ressource VALUES (10,'leon','1994','gaumont','action',5544);
INSERT INTO ressource VALUES (11,'orange mecanique','1971','hawk films','anticipation',3344);
INSERT INTO ressource VALUES (12,'the shining','1980','hawk films','horreur',5457);

-- MUSIQUES

INSERT INTO ressource VALUES (13,'les champs elysees','1969','cbs','variete',1157);
INSERT INTO ressource VALUES (14,'initials bb','1967','philips','variete',5422);
INSERT INTO ressource VALUES (15,'l eau a la bouche','1960','philips','variete',5337);

-- HERITAGE CLASSES FILLES

INSERT INTO livre VALUES (1,'2-7654-1005-4','francais','lorem ipsum');
INSERT INTO livre VALUES (2,'2-8888-5555-4','francais','lorem ipsum');
INSERT INTO livre VALUES (3,'2-9999-4444-4','francais','lorem ipsum');
INSERT INTO livre VALUES (4,'2-0000-1111-4','francais','lorem ipsum');
INSERT INTO livre VALUES (5,'2-2222-8787-4','francais','lorem ipsum');
INSERT INTO livre VALUES (6,'2-1235-6547-4','anglais','lorem ipsum');
INSERT INTO livre VALUES (7,'2-7845-5689-4','anglais','lorem ipsum');

INSERT INTO film VALUES (8,120,'anglais','lorem ipsum');
INSERT INTO film VALUES (9,120,'anglais','lorem ipsum');
INSERT INTO film VALUES (10,135,'francais','lorem ipsum');
INSERT INTO film VALUES (11,140,'anglais','lorem ipsum');
INSERT INTO film VALUES (12,125,'anglais','lorem ipsum');

INSERT INTO Enregistrement VALUES (13,60);
INSERT INTO Enregistrement VALUES (14,65);
INSERT INTO Enregistrement VALUES (15,55);

INSERT INTO exemplaire VALUES (1,1,'neuf');
INSERT INTO exemplaire VALUES (2,1,'bon');
INSERT INTO exemplaire VALUES (3,1,'neuf');
INSERT INTO exemplaire VALUES (4,2,'neuf');
INSERT INTO exemplaire VALUES (5,2,'bon');
INSERT INTO exemplaire VALUES (6,3,'neuf');
INSERT INTO exemplaire VALUES (7,3,'bon');
INSERT INTO exemplaire VALUES (8,3,'neuf');
INSERT INTO exemplaire VALUES (9,4,'abime');
INSERT INTO exemplaire VALUES (10,4,'neuf');
INSERT INTO exemplaire VALUES (11,5,'neuf');
INSERT INTO exemplaire VALUES (12,5,'bon');
INSERT INTO exemplaire VALUES (13,6,'neuf');
INSERT INTO exemplaire VALUES (14,7,'neuf');
INSERT INTO exemplaire VALUES (15,7,'perdu');
INSERT INTO exemplaire VALUES (16,7,'neuf');
INSERT INTO exemplaire VALUES (17,8,'neuf');
INSERT INTO exemplaire VALUES (18,9,'neuf');
INSERT INTO exemplaire VALUES (19,9,'bon');
INSERT INTO exemplaire VALUES (20,10,'bon');
INSERT INTO exemplaire VALUES (21,11,'neuf');
INSERT INTO exemplaire VALUES (22,11,'bon');
INSERT INTO exemplaire VALUES (23,12,'neuf');
INSERT INTO exemplaire VALUES (24,13,'abime');
INSERT INTO exemplaire VALUES (25,13,'neuf');
INSERT INTO exemplaire VALUES (26,13,'neuf');
INSERT INTO exemplaire VALUES (27,14,'bon');
INSERT INTO exemplaire VALUES (28,14,'abime');
INSERT INTO exemplaire VALUES (29,15,'neuf');
INSERT INTO exemplaire VALUES (30,15,'neuf');

INSERT INTO pret VALUES ('wjiang',2,'12-02-2020',7,'bon','bon',0);
INSERT INTO pret VALUES ('wjiang',9,'12-02-2020',7,'neuf','abime',0);
INSERT INTO pret VALUES ('wjiang',11,'20-02-2020',7,'neuf','neuf',0);
INSERT INTO pret VALUES ('bcosson',12,'27-02-2020',7,'bon','bon',0);
INSERT INTO pret VALUES ('bcosson',20,'02-03-2020',7,'bon','bon',0);
INSERT INTO pret VALUES ('tneveux',22,'09-03-2020',7,'bon','bon',0);
INSERT INTO pret VALUES ('tneveux',27,'01-02-2020',7,'bon','bon',0);
INSERT INTO pret VALUES ('tneveux',4,'12-02-2020',7,'neuf','neuf',0);
INSERT INTO pret VALUES ('bcosson',1,'23-03-2020',9,'neuf','neuf',4);
INSERT INTO pret VALUES ('tneveux',3,'30-03-2020',7,'neuf','inconnu',0);

INSERT INTO sanction VALUES ('wjiang',9,'07-04-2020','degradation','sleroy');
