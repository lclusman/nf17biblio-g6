/*donne le titre de chaque ressource et son nombre d'exemplaires*/
DROP VIEW IF EXISTS NB_exemplaires CASCADE;
CREATE VIEW NB_exemplaires AS
SELECT ressource.titre,COUNT(exemplaire.code) FROM exemplaire,ressource
WHERE exemplaire.coderessource=ressource.code
GROUP BY ressource.titre;

/*donne le nombre de prets de chaque adherent*/
DROP VIEW IF EXISTS Prets_Adherent CASCADE;
CREATE VIEW Prets_Adherent AS
SELECT Pret.loginAdherent, COUNT(*)
FROM Pret
GROUP BY Pret.loginAdherent;

/*classe les ressources de la plus empruntee a la moins empruntee (popularite)*/
DROP VIEW IF EXISTS classement_ressource_par_emprunt CASCADE;
CREATE VIEW classement_ressource_par_emprunt AS
SELECT ressource.titre,COUNT(pret.codeexemplaire) FROM ressource, pret, exemplaire
WHERE pret.codeexemplaire= exemplaire.code AND exemplaire.coderessource=ressource.code
GROUP BY ressource.titre
ORDER BY COUNT(pret.codeexemplaire) DESC, ressource.titre ASC;


/*donne le nombre de ressources par genre*/
DROP VIEW IF EXISTS Nb_ressources_par_genre CASCADE;
CREATE VIEW Nb_ressources_par_genre AS
SELECT Ressource.genre, COUNT(*)
FROM Ressource
GROUP BY Ressource.genre;


/*classe les genres prefere d'un adherent (ici tneveux) */
DROP VIEW IF EXISTS genre_prefere_adherent CASCADE;
CREATE VIEW genre_prefere_adherent AS
SELECT ressource.genre, COUNT(pret.codeexemplaire) FROM ressource, pret, exemplaire
WHERE pret.codeexemplaire= exemplaire.code
AND exemplaire.coderessource=ressource.code
AND pret.loginadherent='tneveux'
GROUP BY ressource.genre
ORDER BY COUNT(pret.codeexemplaire) DESC, ressource.genre ASC;

/* indique le nombre de sanctions qu'a eues un adhérent */
DROP VIEW IF EXISTS Nb_sanctions_par_adherent CASCADE;
CREATE VIEW Nb_sanctions_par_adherent AS
SELECT loginAdherent, count(*)
FROM sanction
GROUP BY loginAdherent;

/* indique les exemplaires a rembourser */
DROP VIEW IF EXISTS exemplaires_a_rembourser CASCADE;
CREATE VIEW exemplaires_a_rembourser AS
SELECT pret.loginAdherent, pret.codeexemplaire, pret.etatfinal FROM exemplaire JOIN pret ON exemplaire.code = pret.codeexemplaire
WHERE etat IN ('abime','perdu');

DROP VIEW IF EXISTS exemplaires_disponibles CASCADE;
CREATE VIEW exemplaires_disponibles AS
SELECT exemplaire.code, exemplaire.etat from exemplaire LEFT JOIN pret ON exemplaire.code = pret.codeExemplaire
WHERE pret.datedebut IS NULL
ORDER BY exemplaire.code;
