DROP TABLE IF EXISTS Ressource CASCADE;
DROP TABLE IF EXISTS Exemplaire CASCADE;
DROP TABLE IF EXISTS Film CASCADE;
DROP TABLE IF EXISTS Livre CASCADE;
DROP TABLE IF EXISTS Enregistrement CASCADE;
DROP TABLE IF EXISTS Contributeur CASCADE;
DROP TABLE IF EXISTS Contribution CASCADE;
DROP TABLE IF EXISTS Compte CASCADE;
DROP TABLE IF EXISTS Membre CASCADE;
DROP TABLE IF EXISTS Adherent CASCADE;
DROP TABLE IF EXISTS Pret CASCADE;
DROP TABLE IF EXISTS Sanction CASCADE;

CREATE TABLE Ressource(
code integer PRIMARY KEY,
titre varchar(30),
anneeApparition varchar(4),
editeur varchar(30),
genre varchar(15),
codeClassification integer
);

CREATE TABLE Exemplaire(
code integer PRIMARY KEY,
codeRessource integer REFERENCES Ressource(code),
etat text check (etat IN ('neuf','bon','abime','perdu'))
);


CREATE TABLE Film(
codeRessource integer REFERENCES Ressource(code) PRIMARY KEY,
longueur integer,
langue varchar(30),
synopsis varchar(500)
);

CREATE TABLE Livre(
codeRessource integer REFERENCES Ressource(code) PRIMARY KEY,
ISBN varchar(30),
langue varchar(30),
resume varchar(500)
);

CREATE TABLE Enregistrement (
codeRessource integer REFERENCES Ressource(code) PRIMARY KEY,
longueur integer
);


CREATE TABLE Contributeur (
nom varchar(15),
prenom varchar(15),
ddn date,
nationalite varchar(15),
PRIMARY KEY (nom,prenom)
);

CREATE TABLE Contribution(
id_contribution integer PRIMARY KEY,
codeRessource integer REFERENCES Ressource(code),
nomContributeur varchar(15),
prenomContributeur varchar(15),
foreign key (nomContributeur, prenomContributeur) REFERENCES Contributeur(nom,prenom),
type text CHECK (type in ('auteur', 'acteur','realisateur', 'compositeur', 'interprete'))
);

CREATE TABLE Compte(
login varchar(15) PRIMARY KEY,
mdp varchar
);

CREATE TABLE Membre (
nom varchar(15),
prenom varchar(15),
compte_login varchar(15) REFERENCES Compte(login) primary key,
adresse varchar(30),
email varchar(30)
);

CREATE TABLE Adherent (
nom varchar(15),
prenom varchar(15),
compte_login varchar(15) REFERENCES Compte(login) primary key,
ddn date,
adresse varchar(30),
email varchar(30),
tel varchar(10),
blacklist boolean,
date_abo date,
duree_abo integer
);

-- L'etat final inconnu signifique que l'exemplaire n'est pas encore rendu
CREATE TABLE Pret(
loginAdherent varchar(30),
codeExemplaire integer,
dateDebut date,
duree integer,
etatInitial text CHECK (etatInitial in ('neuf','bon')),
etatFinal text CHECK (etatFinal in ('neuf','bon','abime','perdu','inconnu')),
joursRetard integer,
PRIMARY KEY (loginAdherent, codeExemplaire, dateDebut),
FOREIGN KEY (loginAdherent) REFERENCES Adherent(compte_login),
FOREIGN KEY (codeExemplaire) REFERENCES Exemplaire(code));


CREATE TABLE Sanction(
 loginAdherent varchar(15) references Adherent(compte_login),
 exemplaire integer references Exemplaire(code),
 dateFin date,
 raison text check (raison in ('degradation','perte','retard','autre')),
loginMembre varchar(15) REFERENCES Membre(compte_login),
PRIMARY KEY(exemplaire, dateFin)
);
